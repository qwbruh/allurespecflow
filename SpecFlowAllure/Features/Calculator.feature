﻿Функция: Калькулятор
![Calculator](https://specflow.org/wp-content/uploads/2020/09/calculator.png)
Simple calculator for adding **two** numbers

Link to a feature: [Calculator](SpecFlowAllure/Features/Calculator.feature)
***Further read***: **[Learn more about how to generate Living Documentation](https://docs.specflow.org/projects/specflow-livingdoc/en/latest/LivingDocGenerator/Generating-Documentation.html)**

@mytag
Сценарий: Сложение двух чисел
	Пусть первое число 50
	И второе число 70
	Когда два числа суммируются
	Тогда результат должен быть 126

Сценарий: Приветствие
	Пусть пользователь ввел "Hello"
	Тогда ответ "World2"

Сценарий: Вычитание двух чисел
	Пусть первое число 70
	И второе число 50
	Когда два числа вычитаются
	Тогда результат должен быть 20